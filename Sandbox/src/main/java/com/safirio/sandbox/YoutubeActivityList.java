package com.safirio.sandbox;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Nodza on 11/4/13.
 */
public class YoutubeActivityList extends Activity {

    ListView videoList;
//    String[] videoArray = {"No videos to display"};
    ArrayList<String> videoArrayList = new ArrayList<String>();
    ArrayAdapter<String> videoAdapter;
    Context context;
    String feedUrl = "https://gdata.youtube.com/feeds/api/users/androiddevelopers/playlists?v=2&alt=jsonc&startIndex=1&max-results=20";
//    String feedUrl = "https://allvirtuous.com/api/v1/container_buckets";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yt_list);
        context = this;
//        videoArrayList.add("No videos to display");
        videoList = (ListView) findViewById(R.id.videoList);
//        videoAdapter = new ArrayAdapter<String>(this, R.layout.simple_list_item_1, videoArray);
        videoAdapter = new ArrayAdapter<String>(this, R.layout.yt_list_item, videoArrayList);
        videoList.setAdapter(videoAdapter);

        VideoListTask loaderTask = new VideoListTask();
        loaderTask.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public class VideoListTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(context);
//            dialog.show(context, "Loading videos...", "");
            dialog.setTitle("Loading videos...");
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            // ************ CONNECT AND PULL DATA FROM WEB ******************
            HttpClient client = new DefaultHttpClient();
            HttpGet getRequest = new HttpGet(feedUrl);

            try {
                HttpResponse response = client.execute(getRequest);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();

                if(statusCode != 200) {
                    return null;
                }

                InputStream jsonStream = response.getEntity().getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(jsonStream));
                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                String jsonData = builder.toString();
//                Log.i("JSONData", jsonData);

                // ************ END ******************

                // Create a new JSON object

                JSONObject json = new JSONObject(jsonData);
                JSONObject data = json.getJSONObject("data");
                JSONArray items = data.getJSONArray("items");

                for (int i = 0; i < items.length(); i++) {
                    JSONObject video = items.getJSONObject(i);
//                    String title = video.getString("title");
//                    videoArrayList.add(title);
                    videoArrayList.add(video.getString("title"));
                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            dialog.dismiss();
            videoAdapter.notifyDataSetChanged();
            super.onPostExecute(result);
        }
    }
}
