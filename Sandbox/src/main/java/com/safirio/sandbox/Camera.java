package com.safirio.sandbox;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.net.URI;

/**
 * Created by Nodza on 11/26/13.
 */
public class Camera extends Activity {

    static int TAKE_PICTURE = 1;
    Uri outputFileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
    }

    public void btnTakePhoto(View view) {
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
        outputFileUri = Uri.fromFile(file);

        i.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(i, TAKE_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
            Toast.makeText(this, outputFileUri.toString(), Toast.LENGTH_LONG).show();
        }
    }
}
