package com.safirio.sandbox;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by noelhwande on 11/1/13.
 */
public class JSONActivity extends Activity {

    static String allvirtUrl = "http://aqueous-gorge-4164.herokuapp.com/api/v1/container_buckets.json";
//    Context context;

    static String userId ="";
    static String containerBarcode ="";
    static String location ="";
    static String longitude ="";
    static String latitude ="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json);

//        Button button = (Button)findViewById(R.id.btnPost);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new postMyJSON().execute();
//            }
//        });

        MyAsyncTask getMyAsyncTask = new MyAsyncTask();
        getMyAsyncTask.execute();
    }

    private class MyAsyncTask extends AsyncTask<String, String, String> {

//        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(context);
//            dialog.setTitle("Uploading data...");
//            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            final String body = String.format("{\"container_buckets\": {\"geo_lon\":\"-122.332071\",\"user_id\":\"7\",\"geo_lat\":\"47.606209\",\"location\":\"Seattle, WA\",\"container_barcode\":\"99999999990-12\"}}");

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(allvirtUrl);
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Accept", "application/json");
            try {
                JSONObject jsonObject = new JSONObject();
//                JSONObject queryJSONObject = jsonObject.getJSONObject("container_buckets");

                jsonObject.put("user_id", "2");
                jsonObject.put("container_barcode", "44444440-14");
                jsonObject.put("location", "Boston, MA");
                jsonObject.put("geo_lon", "-122.332071");
                jsonObject.put("geo_lat", "47.606209");
                jsonObject.put("[resources_attributes][][attachment]", new File(Environment.getExternalStorageDirectory().getPath() + "/Download/images.jpeg"));

                StringEntity se = new StringEntity(jsonObject.toString());

//                StringEntity se = new StringEntity(body);
                Log.d("NDH", jsonObject.toString());
//                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                se.setContentType("application/json;charset=UTF-8;image/jpeg");
                httpPost.setEntity(se);

            } catch (JSONException e) {
                Log.e("Error",""+e);
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                Log.e("Error",""+e);
                e.printStackTrace();
            }

            HttpResponse response = null;

            try {
                response = httpClient.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();
                Log.i("NDH", "HTTP Status Code: " + statusLine.getStatusCode());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                Log.e("ClientProtocol",""+e);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("IOE",""+e);
            }

            HttpEntity entity = response.getEntity();

            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (IOException e) {
                    Log.e("IO E",""+e);
                    e.printStackTrace();
                }
            }
            return "Post complete";
        }

        @Override
        protected void onPostExecute(String result) {
//            dialog.dismiss();

            TextView line1 = (TextView) findViewById(R.id.line1);
            TextView line2 = (TextView) findViewById(R.id.line2);
            TextView line3 = (TextView) findViewById(R.id.line3);
            TextView line4 = (TextView) findViewById(R.id.line4);
            TextView line5 = (TextView) findViewById(R.id.line5);

            line1.setText("User ID: " + userId);
            line2.setText("Barcode: " + containerBarcode);
            line3.setText("Location: " + location);
            line4.setText("Longitude: " + longitude);
            line5.setText("Latitude: " + latitude);
        }
    }

//    private String formatDataAsJSON() {
//
//       final JSONObject jsonObject = new JSONObject();
//
//        try {
//            String result = null;
//            JSONObject queryJSONObject = null;
//            JSONObject queryJSONObject = jsonObject.getJSONObject("container_buckets");
//
//                userId = queryJSONObject.getString("user_id");
//                containerBarcode = queryJSONObject.getString("container_barcode");
//                location = queryJSONObject.getString("location");
//                longitude = queryJSONObject.getString("geo_long");
//                latitude = queryJSONObject.getString("geo_lat");
//
//            queryJSONObject.put("user_id", "7");
//            queryJSONObject.put("container_barcode", "99999999990-12");
//            queryJSONObject.put("location", "Seattle, WA");
//            queryJSONObject.put("geo_long", "-122.332071");
//            queryJSONObject.put("geo_lat", "47.606209");
//
//                JSONArray queryArray = queryJSONObject.names();
//                Log.i(JSONActivity.class.getName(),
//                        "Number of entries " + queryArray.length());
//
//                List<String> list = new ArrayList<String>();
//                for (int i = 0; i < queryArray.length(); i++) {
//                    list.add(queryArray.getString(i));
//                }
//
//                for(String item : list) {
//                    Log.v("JSON ARRAY ITEMS", item);
//                }
//
//            return jsonObject.toString(1);
//        } catch (JSONException e1) {
//            Log.d("NDH", "Can't format JSON");
//        }
//        return null;
//    }
//
//
//    private class postMyJSON extends AsyncTask<Void, Void, String>{
//
//        final String json = formatDataAsJSON();

//        new AsyncTask<Void, Void, String>() {

//            @Override
//            protected String doInBackground(Void... params) {
//                return getJSONResponse(json);
//            }
//
//            @Override
//            protected void onPostExecute(String result) {
//
//                TextView line1 = (TextView) findViewById(R.id.line1);
//                TextView line2 = (TextView) findViewById(R.id.line2);
//                TextView line3 = (TextView) findViewById(R.id.line3);
//                TextView line4 = (TextView) findViewById(R.id.line4);
//                TextView line5 = (TextView) findViewById(R.id.line5);
//
//                line1.setText("User ID: " + userId);
//                line2.setText("Barcode: " + containerBarcode);
//                line3.setText("Location: " + location);
//                line4.setText("Longitude: " + longitude);
//                line5.setText("Latitude: " + latitude);
//            }
//        }
//        }.execute();


//    }


//    private String getJSONResponse(String json) {
//
//        HttpPost httpPost = new HttpPost("https://allvirtuous.com/api/v1/container_buckets");
//
//        try {
//            StringEntity entity = new StringEntity(json.toString());
//            entity.setContentType("application/json;charset=UTF-8");
//            httpPost.setEntity(entity);
//            httpPost.setHeader("Content-type", "application/json");
//            httpPost.setHeader("Accept", "application/json");

//            DefaultHttpClient httpClient = new DefaultHttpClient();
//            BasicResponseHandler handler = new BasicResponseHandler();
//
//            String response = httpClient.execute(httpPost, handler);
//
//            return response;
//
//        } catch (UnsupportedEncodingException e){
//            Log.d("NDH", e.toString());
//        } catch (ClientProtocolException e){
//            Log.d("Exception", e.toString());
//        } catch (IOException e){
//            Log.d("Exception", e.toString());
//        }
//
//        return "Unable to contact the server";
//    }




}

// 20:29 http://www.youtube.com/watch?v=qcotbMLjlA4